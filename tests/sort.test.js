import sortByRelevance from "../src/sort.js"

describe("Example tests", () => {
    test("1 + 2 = 3", () => {
        expect(1 + 2).toBe(3)
    })
})

describe(`
  GIVEN a list of programs
  WHEN I sort them by relevance
  EXPECT the programs be in the expected order`, () => {
    test("single program", () => {
        const program = {
            title: "Estim'action",
            nature: "financement",
            sectors: ["industrie", "tertiaire"],
        }
        const programs = [program]

        // expect(sortByRelevance(programs)[0]).toBe(program)
        // expect(sortByRelevance(programs)[0]).toStrictEqual(program)
    })
})
