/** Sort programs by relavance.
 *
 * @typedef {Object} Program
 * @property {string} title
 * @property {string} nature : valeur possibles "accompagnement", "financement", "prêt" et "formation".
 * @property {string} cost : chaîne de caractères qui donne le coût reste à charge pour l'entreprise
 * @property {Array<string>} sectors : secteurs d'activité ciblés
 *
 * @param {Array<Program>} _programs
 *
 * Cette fonction tri les programmes par pertinence de la manière suivante :
 * 1/ les financements
 * 2/ les prêts
 * 3/ les accompagnements et formations payantes (hors 4/ et 5/)
 * 4/ les accompagnements et les formations gratuits sous conditions (coût reste à charge `cost` contient le mot "gratuit" ou "Gratuit").
 * 5/ les accompagnements et les formations gratuits (coût reste à charge
 * `cost` prend la valeur "gratuit" ou "Gratuit")
 */
export default function sortByRelevance(_programs) {
    return []
}
