import sortByRelevance from "./sort.js"

const programs = [
    {
        title: "Tremplin",
        nature: "financement",
        sectors: ["tertiaire", "tourisme", "artisanat", "industrie"],
    },
    {
        title: "Fonds tourisme durable",
        nature: "accompagenement",
        sectors: ["tourisme"],
        cost: "Gratuit",
    },
    {
        title: "Prêt vert ADEME",
        nature: "prêt",
        sectors: ["artisanat", "industrie"],
    },
    {
        title: "Aides au réemploi des emballages",
        nature: "financement",
        sectors: ["tertiaire", "tourisme", "artisanat", "industrie"],
    },
    {
        title: "Formations RSE",
        nature: "formation",
        sectors: ["tertiaire", "tourisme", "artisanat", "industrie"],
        cost: "Gratuit (sauf en Bretagne et Grand Est)",
    },
    {
        title: "Réparacteur",
        nature: "accompagenement",
        sectors: ["artisanat"],
        cost: "Gratuit",
    },
]

const sortedPrograms = sortByRelevance(programs)

console.log("Programmes triés (index.js)")
console.log()
console.log(sortedPrograms)
